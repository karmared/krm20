module.exports = {
  ethereum: {
    provider: "ws://127.0.0.1:8546",
    contract: "contract.json",
    rpcport: 8545,
    account: "0x632D901dD69eE3fb94BCbcAC3E12F9F5f4D3B60b",
    pass: "123",
    maxGasPrice: 50 * 10 ** 9,
    minGasPrice: 10 ** 9,
    confirmations: 10,
    notifyBalanceTx: 100,
    waitTx: 12 * 60 * 60 * 1000
  },
  chain: {
    node: "wss://node.karma.red",
    account: "krm-eth",
    pass: "krm-eth",
    confirmations: 2,
    feeKRM: 20 * 10 ** 5
  },
  minKRM: 100 * 10 ** 5,
  gasFromKRM: 80000,
  gasToKRM: 40000,

  database: "database.sqlite",
  port: 3000
}