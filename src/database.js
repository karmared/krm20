const Sequelize = require('sequelize');

class Database {
  constructor(baseName) {
    const sequelize = new Sequelize('database', 'user', 'user',{
      dialect: 'sqlite',
      storage: baseName,
      //logging: false
    })

    this.User = sequelize.define('user', {
      eth: {type: Sequelize.STRING},
      karma: {type: Sequelize.STRING}
    })

    this.User.sync()

    this.event = sequelize.define('event', {
      direction: {type: Sequelize.STRING},
      block: {type: Sequelize.STRING},
      fromId: {type: Sequelize.STRING},
      toId: {type: Sequelize.STRING},
      status: {type: Sequelize.STRING}
    })

    this.event.sync()
  }

  newUser(eth, karma) {
    return this.User.create({eth, karma})
  }

  newEvent(direction, fromId, block) {
    return this.event.create({direction, fromId, block, status: "detect"})
  }

  lastEvent(direction) {
    return this.event.findOne({order: [['createdAt', "DESC"]], where: {direction}})
  }

  async ethToKarma(addr) {
    let acc = await this.User.findOne({where: {eth: addr.toLowerCase()}, attributes: ['karma'] })
    return acc ? acc.karma : null
  }

  async karmaToEth(account) {
    let addr = await this.User.findOne({where: {karma: account.toLowerCase()}, attributes: ['eth'] })
    return addr ? addr.eth : null
  }
}

module.exports = Database;