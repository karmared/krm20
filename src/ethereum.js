const Web3 = require("web3"),
  jayson = require("jayson/promise"),
  fs = require("fs"),
  slack = require("../src/logger"),
  logger = require("karma-logger-node")(require("path").basename(__filename)),
  CryptoJS = require("crypto-js"),
  EthereumTx = require("ethereumjs-tx");

class Ethereum {
  constructor(config, providerBackup) {
    this.config = config;
    this.contract = JSON.parse(fs.readFileSync(config.contract));
    this.addresses = [];
    this.confirmedBlock = 0;
    this.minKRM = 0;
    this.eventQueue = Promise.resolve();
    this.notifyBalance = 0;
    this.queues = {};
    this.providerBackup = providerBackup;

    this.burn = options =>
      this.createTx("burn", ["address", "uint256"], options);
    this.mint = options =>
      this.createTx("mint", ["address", "uint256"], options);
  }

  async connect() {
    this.web3 = new Web3(this.providerBackup);
    this.web3.eth.accounts.wallet.add(this.config.pass);

    this.contract = new this.web3.eth.Contract(
      this.contract.abi,
      this.contract.address
    );

    this.checkBalance();
  }

  setDB(db) {
    this.db = db;
  }

  setMinKRM(minAmount) {
    this.minKRM = minAmount;
  }

  setNotifyBalance(wei) {
    this.notifyBalance = wei;
  }

  async checkBalance() {
    let balance = await this.web3.eth.getBalance(this.config.account);
    if (balance < this.notifyBalance) {
      slack.warning(
        `Balance saleAgent: ${this.web3.utils.fromWei(balance)} ETH`
      );
      logger.info(`Balance saleAgent: ${this.web3.utils.fromWei(balance)} ETH`);
    }
  }

  async subscribe(callback) {
    if (!this.contract) throw new Error("Don't connect to blockchain");

    this.callback = callback;

    // watch blocks when I sleep
    this.confirmedBlock =
      (await this.web3.eth.getBlockNumber()) - this.config.confirmations;
    let lastEvent = await this.db.lastEvent("eth->karma");

    this.checkEventsInBlock(this.confirmedBlock, lastEvent && lastEvent.block);

    // watch confirmed blocks
    this.web3.eth.subscribe("newBlockHeaders").on("data", blockHeader => {
      if (
        this.confirmedBlock <
        blockHeader.number - this.config.confirmations
      ) {
        this.confirmedBlock = blockHeader.number - this.config.confirmations;
        this.checkEventsInBlock(this.confirmedBlock);
      }
    });
  }

  async checkEventsInBlock(block, fromBlock) {
    let events = await this.contract
      .getPastEvents("Transfer", {
        fromBlock: fromBlock || block,
        toBlock: block
      })
      .catch(e => []);

    events.forEach(event => {
      if (this.addresses.includes(event.returnValues.to.toLowerCase())) {
        this.eventQueue = this.eventQueue.then(() => this.registerEvent(event));
      }
    });
  }

  async registerEvent(event) {
    if (
      await this.db.event.findOne({
        where: { fromId: event.id, block: event.blockNumber }
      })
    ) {
      logger.info("Event find in db");
      return;
    }

    let eventDB = await this.db.newEvent(
      "eth->karma",
      event.id,
      event.blockNumber
    );
    //this.processEvent(event, eventDB)
    //let to = event.returnValues.to;
    this.queues = this.queues || Promise.resolve();

    this.queues = this.queues.then(() => this.processEvent(event, eventDB));
  }

  async processEvent(event, eventDB) {
    let value = event.returnValues.value,
      to = event.returnValues.to;

    let balance = await this.contract.methods
      .balanceOf(to)
      .call({ from: this.config.account });
    if (balance < this.minKRM) {
      logger.info(`balance < minKRM: ${balance} < ${this.minKRM}`);
      return;
    }

    let gasPrice = await this.getGasPrice();
    logger.info(`burn(${to}, ${balance}) with gasPrice: ${gasPrice}`);

    let [txHash, burnPromise] = await this.watchTx(
      await this.burn({ from: this.config.account, gasPrice })(to, balance), //this.contract.methods.burn(to, balance),
      { from: this.config.account, gasPrice }
    ).catch(console.error);
    let status = null;

    if (txHash === null) {
      balance = await this.contract.methods
        .balanceOf(to)
        .call({ from: this.config.account });
      if (balance < this.minKRM) {
        logger.info(`balance < minKRM: ${balance} < ${this.minKRM}`);
        return;
      }
      status = "error";
    } else status = await burnPromise.catch(error => "error");

    if (status == "error" || status == false) {
      await eventDB.update({ status: "timeout" });
      logger.error(
        `Burn tx timeout: to ${to}, balance burn ${balance}, gasPrice ${gasPrice}, hash ${txHash}`
      );
      slack.error(
        `Burn tx timeout: to ${to}, balance burn ${balance}, gasPrice ${gasPrice}, hash ${txHash}`
      );
      return;
    }

    await eventDB.update({ status: "burnt" });

    logger.info(`eth->karma: ${to} -> ${balance}`);
    try {
      let txId = await this.callback({ to, value: balance });

      await eventDB.update({ toId: txId[0].id, status: "transfered" });
    } catch (error) {
      logger.info(error);
      return;
    }

    this.checkBalance();
  }

  async transfer(address, amount) {
    if (this.contract && this.web3.utils.isAddress(address)) {
      let gasPrice = await this.getGasPrice();
      logger.info(`mint(${address}, ${amount}) with gasPrice: ${gasPrice}`);

      this.checkBalance();

      return this.watchTx(
        await this.mint({ from: this.config.account, gasPrice })(
          address,
          amount
        ),
        {}
      );
    }
  }

  async watchTx(txPromise, options) {
    let txHash,
      minePromise,
      start = Date.now();
    let ethereum = this;

    let waitHash = new Promise((rs, rj) => {
      this.web3.eth
        .sendSignedTransaction(txPromise)
        .on("transactionHash", hash => {
          logger.info(`txHash: ${hash}`);

          txHash = hash;

          minePromise = new Promise((resolve, reject) => {
            let timer = setInterval(async () => {
              let receipt = await ethereum.web3.eth.getTransactionReceipt(hash);

              logger.info(`check hash for ${hash}`, receipt);
              if (
                receipt != null ||
                Date.now() > start + ethereum.config.waitTx
              ) {
                clearInterval(timer);
                receipt != null ? resolve(receipt.status) : reject();
              }
            }, 10000);
          });

          rs();
        })
        .on("error", error =>
          logger.error(`MINE ${/*error.toString()*/ "infura"}`)
        )
        .catch(error => {
          // here if 50 block not mine or tr mine with false status
          logger.error(`tx error (${JSON.stringify(options)}): ${error}`);
        });
    });

    try {
      await waitHash;
    } catch (error) {
      console.log("catch here");
      return [null, error];
    }

    return [txHash, minePromise];
  }

  async getGasPrice() {
    let gasPrice = await this.web3.eth.getGasPrice();
    logger.info(`call getGasPrice: ${gasPrice}`);
    return Math.min(
      Math.max(gasPrice * 2, this.config.minGasPrice),
      this.config.maxGasPrice
    );
  }

  generate_address(accName) {
    return this.web3.eth.accounts
      .create(this.web3.utils.randomHex(32))
      .address.toLowerCase();
  }

  addAddress(addr) {
    if (this.web3.utils.isAddress(addr))
      this.addresses.push(addr.toLowerCase());
  }

  createTx(funName, types, options) {
    logger.info(`call createTx with: ${JSON.stringify(options)}`);
    return async (...args) => {
      logger.info(`call ${funName}: ${args}`);
      var privateKey = Buffer.from(this.config.pass.substring(2), "hex");

      var fullName = funName + "(" + types.join() + ")";
      var signature = CryptoJS.SHA3(fullName, { outputLength: 256 })
        .toString(CryptoJS.enc.Hex)
        .slice(0, 8);
      var dataHex =
        signature +
        this.web3.eth.abi.encodeParameters(types, args).substring(2);
      var data = "0x" + dataHex;

      var rawTx = {
        nonce: this.web3.utils.toHex(
          await this.web3.eth
            .getTransactionCount(this.config.account)
            .then(data => data)
        ),
        gasPrice: this.web3.utils.toHex(options.gasPrice),
        gasLimit: this.web3.utils.toHex(8 * 10 ** 5),
        // from: '',
        to: this.contract.address,
        value: 0,
        data
      };

      logger.info(JSON.stringify(rawTx));
      var tx = new EthereumTx(rawTx);
      tx.sign(privateKey);
      var serializedTx = "0x" + tx.serialize().toString("hex");

      return serializedTx;
    };
  }
}

module.exports = Ethereum;
