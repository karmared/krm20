const 
  {Apis, ChainConfig} = require('karmajs-ws'),
  {TransactionBuilder, TransactionHelper, PrivateKey, Aes} = require('karmajs'),
  slack = require('../src/logger'),
  logger = require('karma-logger-node')(require('path').basename(__filename));

function getApi(apiName) {
  let api = {
    get(api, method) {
      return function() {
        //console.log(`${apiName}: ${method}(${[...arguments]})`)
        return Apis.instance()[apiName]().exec(method,[...arguments])
      }
    }
  }
  return new Proxy(api,api)
}

class Chain {
  constructor(config, blacklist, blockAmountTransfer) {

    if (process.env.TESTNET) {
      ChainConfig.networks.Karma = {
        core_asset: 'KRMT',
        address_prefix: 'KRMT',
        chain_id: 'e81bea67cebfe8612010fc7c26702bce10dc53f05c57ee6d5b720bbe62e51bef'
      }
      ChainConfig.setPrefix('KRMT')
    }

    this.config = config


    this.activeKey = PrivateKey.fromSeed(`${config.account}active${config.pass}`)
    this.memoKey = this.activeKey //PrivateKey.fromSeed(`${config.account}memo${config.pass}`)
    this.account = null
    this.historyLast = "1.11.0"
    this.confirmedBlock = 0
    this.minKRM = 0
    this.checkQueue = Promise.resolve()
    this.blacklist = blacklist
    this.blockAmountTransfer = blockAmountTransfer

    Apis.setRpcConnectionStatusCallback(this.statusCallBack.bind(this))
  }

  statusCallBack(status) {
    if (status === 'closed') {
      logger.info("Status connection (karma): closed")
      setTimeout(this.connect()
        .catch(error => logger.error(error)), 1000)
    }
  }

  async connect() {
    let res = await Apis.instance(this.config.node, true).init_promise;
    logger.info("connected to:", res[0].network);

    this.db_api = getApi("db_api")
    this.history_api = getApi("history_api")
    this.account = await this.db_api.get_account_by_name(this.config.account)

    //check account
    if (this.account.active.key_auths[0][0] !== this.activeKey.toPublicKey().toPublicKeyString() ||
        this.account.options.memo_key !== this.memoKey.toPublicKey().toPublicKeyString())
      throw new Error("Accountin Karma Blockchain don't correct")
  }

  setDB(db) {
    this.db = db
  }

  setMinKRM(minAmount) {
    this.minKRM = minAmount
  }

  setEther(ether) {
    this.ether = ether
  }

  setFeeFunc(feeFunc) {
    this.feeFunc = feeFunc
  }

  async subscribe(callback) {
    this.callback = callback

    // watch blocks when I sleep
    let lastEvent = await this.db.lastEvent("karma->eth")
    this.historyLast = lastEvent ? lastEvent.fromId : (await this.history_api.get_account_history(this.account.id, "1.11.0", 1, "1.11.0"))[0].id;

    await this.db_api.set_subscribe_callback(this.update.bind(this), true)
    await this.db_api.get_full_accounts([this.config.account], true)

    await this.checkTrasfers()
  }

  async update(updates) {
    updates.forEach(array => array.forEach(obj => {
      if (obj.id) {
        if (obj.id == '2.1.0') { // new block
          this.confirmedBlock = obj.head_block_number - this.config.confirmations
        }
        else if (/^2\.5\./.test(obj.id) &&
                  obj.owner == this.account.id &&
                  obj.asset_type == '1.3.0') {
          logger.info(`balanceChange: ${JSON.stringify(obj)}`)
          this.checkQueue = this.checkQueue.then(this.checkTrasfers.bind(this))
        }
      }
    }))
  }

  async checkTrasfers() {
    let history = await this.history_api.get_account_history(this.account.id, this.historyLast, 100, "1.11.0");

    await Promise.all(history.reverse().map( async (h) => {
      if ( h.id === this.historyLast)
        return

      let op = h.op
      if (op[0] == 0 && op[1].to === this.account.id && op[1].amount.asset_id === '1.3.0') {
        if (this.historyLast.replace(/1\.11\.(\d+)/,"$1") < h.id.replace(/1\.11\.(\d+)/,"$1"))
          this.historyLast = h.id

        if (await this.db.event.findOne({where: {fromId: h.id} })) {
          logger.info("Event find in db")
          return
        }

        try {
          await this.waitConfirm(h)

          let event = await this.db.newEvent("karma->eth", h.id, h.block_num);

          this.processOperation(op[1], event)
        } catch(error) {
          logger.info(error)
        }
      }
    }))
  }

  async processOperation(op, event) {
    if (op.amount.amount < this.minKRM) {
      logger.info(`amount < min: ${op.amount.amount} < ${this.minKRM}`)
      let [tx] = await this.returnTokens(op)
      tx && await event.update({toId: tx.block_num, status: 'return'})
      return
    }

    let memo = op.memo;
    if (!memo) {
      logger.info("Not have memo!")
      let [tx] = await this.returnTokens(op)
      tx && await event.update({toId: tx.block_num, status: 'return'})
      return
    }

    let
      [from] = await this.db_api.get_accounts([op.from]),
      addr;

    try {
      addr = Aes.decrypt_with_checksum(this.activeKey, from.options.memo_key, memo.nonce, memo.message).toString()
                  .replace(/ |\n|\t/g,"");
    } catch(error) {
      logger.info("Not decrypt memo!")
      let [tx] = await this.returnTokens(op)
      tx && await event.update({toId: tx.block_num, status: 'return'})
      return
    }

    if (!this.ether.web3.utils.isAddress(addr)) {
      logger.info(`Memo is not an address: ${addr}`)
      let [tx] = await this.returnTokens(op)
      tx && await event.update({toId: tx.block_num, status: 'return'})
      return
    }

    let amount = op.amount.amount - (await this.feeFunc()).feeFromKRM;

    logger.info(`karma->eth: '${op.from}' -> ${amount} to ${addr}`)

    if (this.blacklist.includes(from.name) && amount >= this.blockAmountTransfer) {
      logger.info(`karma->eth: blacklist account`)
      return
    }

    if (amount <= 0) {
      await event.update({status: 'zero_amount'})
      return
    }

    let [toId, mineTx] = await this.callback({addr, amount})
    await event.update({toId})

    let result = await mineTx.catch(() => "error")

    if (result === "error") {
      await event.update({status: 'timeout'});
      logger.error(`Mint tx timeout: to ${addr}, balance mint ${amount}, toId: ${toId}`)
      slack.error(`Mint tx timeout: to ${addr}, balance mint ${amount}, toId: ${toId}`)

      return
      /*
      [toId, mineTx] = await this.callback({addr, amount})
      await event.update({toId})

      result = await mineTx.catch(() => "error")

      if (result === "error") {
        logger.info(`Not mine transfer: ${addr}`)
        //let [tx] = await this.returnTokens(op)
        //tx && await event.update({toId: tx.block_num, status: 'return'})

        return
      }
      */
    }

    await event.update({status: result ? 'minted' : 'canceled'})
  }

  async returnTokens(op) {
    let fee = await this.getTransferFee()

    if (op.amount.amount > fee) {
      logger.info(`Return ${op.amount.amount - fee} KRM`)
      let name = (await this.db_api.get_accounts([op.from]))[0].name
      return this.transfer(name, op.amount.amount, fee)
    }
  }

  async getTransferFee() {
    let prop = await this.db_api.get_global_properties()
    return prop.parameters.current_fees.parameters[0][1].fee;
  }

  waitConfirm(history) {
    return new Promise((resolve, reject) => {
      let wait = setInterval(async () => {
        if (this.confirmedBlock >= history.block_num) {
          clearInterval(wait)

          let transaction = await this.db_api.get_transaction(history.block_num, history.trx_in_block),
              op = JSON.stringify(transaction.operations[history.op_in_trx]);
          
          (op === JSON.stringify(history.op)) ? resolve() : reject()
        }
      }, 3000)
    })
  }

  async transfer(acc, amount, feeKRM) {
    if (this.blacklist.includes(acc) && amount >= this.blockAmountTransfer)
      return [{id: 'blacklist'}]

    if (feeKRM == undefined)
      feeKRM = (await this.feeFunc()).feeToKRM;

    logger.info(`transfer ${acc} ${amount} and fee: ${feeKRM}`)
    let accDB = await this.db_api.get_account_by_name(acc)

    if (!accDB || accDB.name !== acc)
      throw new Error(`Not found account ${accDB}! Blockchain return ${accDB ? accDB.name : accDB}`)

    let operation = {
      fee: {asset_id: '1.3.0', amount: 0},
      from: this.account.id,
      to: accDB.id,
      amount: {asset_id: '1.3.0', amount: (amount - feeKRM)},
      extensions: [],
    };

    let tx = new TransactionBuilder();
    tx.add_type_operation("transfer", operation)
    await tx.set_required_fees()
    tx.add_signer(this.activeKey, this.activeKey.toPublicKey().toPublicKeyString());
    return tx.broadcast();
  }

  async isAccExist(name) {
    return !!(await this.db_api.get_account_by_name(name))
  }
}

module.exports = Chain