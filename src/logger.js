const SlackBot = require("slackbots")
 
// create a bot
var bot = new SlackBot({
    token: 'xoxb-298908272147-384151184342-SMHM069ctFNEG9QRcgupG3y5', // Add a bot https://my.slack.com/services/new/bot and put the token 
    name: 'krm20'
});

var database = null;
 
bot.on('start', async function() {
  let users = (await bot.getUsers()).members
  //console.log(users)
  bot.on('message', async function(data) {
    // all ingoing events https://api.slack.com/rtm
    //console.log("message", data);
    

    if (data.type === 'message') {
      let user = users.find(user => user.id === data.user)
      if (!user)
        return

      try {
        if (/^database /.test(data.text)) {
          let request = JSON.parse(data.text.replace("database ",''))

          let result = ''
          if (request.user) {
            result = await database.User.findAll(request.user)
            bot.postMessageToUser(user.name, "```" + JSON.stringify(result) + "```", { icon_emoji: ':man:' })
          }

          if (request.event) {
            result = await database.event.findAll(request.event)
            bot.postMessageToUser(user.name, `\`\`\`${JSON.stringify(result)}\`\`\``, { icon_emoji: ':heavy_check_mark:' })
          }
        } else bot.postMessageToUser(user.name, data.text, { icon_emoji: ':rage:' })
      } catch(error) {
        console.log(error.message)
        try {
          bot.postMessageToUser(user.name, `\`${error.message}\``, { icon_emoji: ':heavy_check_mark:' })
        } catch(error) {}
      }
      
    } 
  })
});

module.exports = {
  warning: txt => bot.postMessageToChannel('krm20-platform-karma', txt, { icon_emoji: ':warning:' }),
  error: txt => bot.postMessageToChannel('krm20-platform-karma', txt, { icon_emoji: ':fire:' }),
  info: txt => bot.postMessageToChannel('krm20-platform-karma', txt, { icon_emoji: ':cat:' }),
  setDB: db => database = db
}

