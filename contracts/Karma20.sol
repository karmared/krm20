pragma solidity ^0.4.21;

import "./zeppelin/contracts/token/ERC20/BurnableToken.sol";
import "./zeppelin/contracts/token/ERC20/StandardToken.sol";
import "./zeppelin/contracts/ownership/Ownable.sol";

contract Karma20 is StandardToken, BurnableToken, Ownable {
  string public constant name = "Karma";
  string public constant symbol = "KRM";
  uint32 public constant decimals = 5;

  address public saleAgent;

  event Mint(address indexed to, uint256 amount);

  modifier onlyAgent() {
    require (saleAgent == msg.sender); 
    _;
  }

  function setSaleAgent(address agent) public onlyOwner {
    require (agent != address(0));
    saleAgent = agent;
  }

  function mint(address _to, uint256 _amount) onlyAgent public returns (bool) {
    totalSupply_ = totalSupply_.add(_amount);
    balances[_to] = balances[_to].add(_amount);
    emit Mint(_to, _amount);
    emit Transfer(address(0), _to, _amount);
    return true;
  }

  function burn(address _who, uint _amount) onlyAgent public returns (bool) {
    _burn(_who, _amount);
    return true;
  }

  function transfer(address _to, uint256 _value) public returns (bool) {
    require(_to != address(0) && _to != address(this));
    require(_value <= balances[msg.sender]);

    balances[msg.sender] = balances[msg.sender].sub(_value);
    balances[_to] = balances[_to].add(_value);
    emit Transfer(msg.sender, _to, _value);
    return true;
  }
}