module.exports = {
  ethereum: {
    provider: "ws://127.0.0.1:8546",
    contract: "contract-testnet.json",
    rpcport: 8545,
    account: "0x57a6e681f6177ad39cae914975e4855c4854e9f1",
    pass: "123",
    maxGasPrice: 50 * 10 ** 9,
    confirmations: 10,
    notifyBalanceTx: 100,
    waitTx: 1 * 60 * 60 * 1000
  },
  chain: {
    node: "wss://testnet-node.karma.red",
    account: "gateway-2",
    pass: "gateway-2",
    confirmations: 2,
    feeKRM: 1 * 10 ** 5
  },
  minKRM: 10 * 10 ** 5,
  gasFromKRM: 80000,
  gasToKRM: 40000,

  database: "database.sqlite",
  port: 3000
}