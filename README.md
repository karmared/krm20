# Karma token and  middleware for Ethereum blockchain
Karma token write on Solidity, and use zeppelin contracts.

## How to start moddleware
KRM20 consists of two parts that must be run:
  1. geth v1.8.9
  2. service.js

### How to start geth:
  1. [Download](https://geth.ethereum.org/downloads/) and exctract geth-archive (for example, in the folder `geth`)
  2. In the folder `geth` we create a folder `rinkeby`
  3. In the `geth` folder, run the command:
```
$ ./geth --rinkeby --rpc --rpcapi db, eth, net, web3, personal, debug, miner, txpool --datadir =. / Rinkeby console --fast - -ws --wsport 8546 --wsaddr 0.0.0.0 --wsorigins "*" --rpcport 8545
```

  4. geth start with the console, i.e. there you can enter commands. You must create a new account by writing:
```
> personal.newAccount(<password>)
```

The command will return the address (something like `0x7e01da2FFf1aC744e30D8e2164cdFb604c8BBe85`)

### How to start service.js:
1. Run command:
```
git clone https: //scientistnik-red@bitbucket.org/karmared/krm20.git
```
2. In the folder `krm20` there is a file `config.js`, where you need to assign a port for the service (last line `port`). There is 3000. It is necessary to see which port is available on the server.

Also in `config.js` you need to enter the password and the address received in the previous part:
```
ethereum: {
   ... 
   account: '0x7e01da2FFf1aC744e30D8e2164cdFb604c8BBe85', 
   pass: '123',
 }
```
This account need set in smartcontract how saleAgent.