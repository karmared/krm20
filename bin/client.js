#!/usr/bin/env node

var jayson = require('jayson');

let server = 'http://localhost:3000'
//let server = 'http://159.69.23.139:3000'

var client = jayson.client.http(server);

let accName = process.argv[2] || 'test-01'

client.request('get_address', [accName], function(err, response) {
  if(err) throw err;
  console.log(`${accName}: ${response.result}`);
});