#!/usr/bin/env node

require("dotenv").config();

const config = process.env.TESTNET
    ? require("../config-testnet.js")
    : require("../config.js"),
  Chain = require("../src/karmachain.js"),
  Ethereum = require("../src/ethereum.js"),
  Database = require("../src/database.js"),
  jayson = require("jayson/promise"),
  axios = require("axios"),
  slack = require("../src/logger.js"),
  logger = require("karma-logger-node")(require("path").basename(__filename));

var ethereum, chain;

var arrFeeFrom = new Array(6),
  arrFeeTo = new Array(6),
  feeFrom = config.chain.feeKRM,
  feeTo = config.chain.feeKRM;

main();

async function main() {
  chain = new Chain(
    config.chain,
    process.env.BLACKLIST_ACCOUNTS.split(","),
    process.env.BLACKLIST_BLOCK_AMOUNT_TRANSFER
  );

  ethereum = new Ethereum(config.ethereum, process.env.WEB3_PROVIDER_BACKUP);
  let db = new Database(config.database);

  ethereum.setDB(db);
  chain.setDB(db);
  slack.setDB(db);

  ethereum.setMinKRM(config.minKRM);
  chain.setMinKRM(config.minKRM);

  chain.setEther(ethereum);
  chain.setFeeFunc(getFee);

  ethereum.setNotifyBalance(
    Math.max(config.gasFromKRM, config.gasToKRM) *
      config.ethereum.maxGasPrice *
      config.ethereum.notifyBalanceTx
  );

  try {
    await ethereum.connect();
    await chain.connect();
  } catch (error) {
    logger.info(error.toString());
    process.exit(1);
  }

  // add listen addresses
  let users = await db.User.findAll();
  users.forEach(user => ethereum.addAddress(user.eth));

  ethereum
    .subscribe(async event =>
      chain.transfer(await db.ethToKarma(event.to), event.value)
    )
    .catch(console.error);
  chain.subscribe(async event => ethereum.transfer(event.addr, event.amount));

  var server = jayson.server({
    async get_address([accName]) {
      let addr = await db.karmaToEth(accName);

      if (!addr) {
        if (!(await chain.isAccExist(accName))) {
          let msg = `account ${accName} don't found`;
          logger.error(msg);
          return { error: msg };
        }

        addr = ethereum.generate_address(accName);
        db.newUser(addr, accName);
        ethereum.addAddress(addr);
      }

      return addr;
    },
    async get_params() {
      let { feeFromKRM, feeToKRM } = getFee();

      return [
        {
          account: config.chain.account,
          network: "ETH",
          minKRM: Math.max(config.minKRM, feeFromKRM, feeToKRM),
          feeFromKRM,
          feeToKRM,
          defaultFeeKRM: config.chain.feeKRM
        }
      ];
    },
    async get_tx_state(ids) {
      let result = new Array(ids.length);

      for (let i in ids) {
        let fromId = ids[i],
          attributes = ["fromId", "toId", "status", "updatedAt"];

        if (typeof fromId === "string") {
          if (/\d\.\d+\.\d+/.test(fromId))
            // karma -> eth
            result[i] = await db.event.findOne({
              where: { fromId },
              attributes
            });
          else if (fromId.length == 66 && /0x\w*/.test(fromId)) {
            // eth -> karma (txHash)
            let receipt = await ethereum.web3.eth.getTransactionReceipt(fromId);

            if (receipt)
              result[i] = await db.event.findOne({
                where: { fromId: receipt.logs.map(log => log.id) },
                attributes
              });
          } // eth -> karma (log ID)
          else
            result[i] = await db.event.findOne({
              where: { fromId },
              attributes
            });
        }
      }

      return result;
    },
    async get_version() {
      return require("../package.json").version;
    },

    async get_database(query) {
      try {
        let result = null;
        if (query.user) {
          result = await db.User.findAll(query.user);
        }

        if (query.event) {
          result = await db.event.findAll(query.event);
        }

        return result;
      } catch (error) {
        return error.toString();
      }
    }
  });

  server.http().listen(config.port);

  setInterval(calcFee, 10 * 60 * 1000);
}

async function calcFee() {
  try {
    let eth_krm = await axios.get(
        `https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=ETH&convert=KRM&CMC_PRO_API_KEY=${
          process.env.CMC_PRO_API_KEY
        }`
      ),
      price = eth_krm.data.data.ETH.quote.KRM.price,
      ethGasPrice = await ethereum.getGasPrice(),
      transferFee = await chain.getTransferFee();

    feeFrom =
      Math.ceil((config.gasFromKRM * ethGasPrice * price) / 10 ** 18) * 10 ** 5;
    feeTo =
      Math.ceil((config.gasToKRM * ethGasPrice * price) / 10 ** 18) * 10 ** 5 +
      transferFee;
  } catch (error) {
    logger.info("Coinmarketcap error");
  }

  arrFeeTo.shift();
  arrFeeTo.push(feeTo);

  arrFeeFrom.shift();
  arrFeeFrom.push(feeFrom);
}

function getFee() {
  let feeFromKRM = 0,
    feeToKRM = 0;

  for (let i = 0; i < arrFeeTo.length; i++) {
    feeFromKRM += arrFeeFrom[i] || config.chain.feeKRM;
    feeToKRM += arrFeeTo[i] || config.chain.feeKRM;
  }

  feeFromKRM /= arrFeeFrom.length;
  feeToKRM /= arrFeeTo.length;

  feeFromKRM = Math.ceil(feeFromKRM / 10 ** 5) * 10 ** 5;
  feeToKRM = Math.ceil(feeToKRM / 10 ** 5) * 10 ** 5;

  return { feeFromKRM, feeToKRM };
}

process.on("unhandledRejection", (reason, p) => {
  let error = `Unhandled Rejection at: ${p} reason: ${reason}`;
  logger.error("Unhandled Rejection", p, reason);
});
